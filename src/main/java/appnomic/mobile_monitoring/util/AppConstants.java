package appnomic.mobile_monitoring.util;

import java.util.HashMap;
import java.util.Map;

public class AppConstants {
	public static final String CRASHLYTICS_TABLE = " `sample-appnomic.firebase_crashlytics.org_codejudge_android_ANDROID` ";
	public static final String PERFMON_TABLE = " `sample-appnomic.firebase_performance.org_codejudge_android_ANDROID` ";
	public static final String ANALYTICS_TABLE = " `sample-appnomic.analytics_266548821.events_20210420` ";
	
	public static final Long MILLIS_THIRTY_MINUTE = 1800000L;
	public static final Long MILLIS_ONE_HOUR = 3600000L;
	public static final Long MILLIS_FOUR_HOURS = 14400000L;
	public static final Long MILLIS_TWELVE_HOURS = 43200000L;
	public static final Long MILLIS_TWENTY_FOUR_HOURS = 86400000L;
	public static final Long MILLIS_ONE_WEEK = 604800000L;
	public static final Long MILLIS_ONE_MONTH = 2592000000L;
	
	public static final String BUCKET_THIRTY_MINUTE = "1 MINUTE";
	public static final String BUCKET_ONE_HOUR = "1 MINUTE";
	public static final String BUCKET_FOUR_HOURS = "15 MINUTE";
	public static final String BUCKET_TWELVE_HOURS = "30 MINUTE";
	public static final String BUCKET_TWENTY_FOUR_HOURS = "1 HOUR";
	public static final String BUCKET_ONE_WEEK = "1 DAY";
	public static final String BUCKET_ONE_MONTH = "1 DAY";
	
	public static Map<Long, String> getBucketMap() {
		Map<Long, String> bucketValue = new HashMap<Long, String>();
		bucketValue.put(MILLIS_THIRTY_MINUTE, BUCKET_THIRTY_MINUTE);
		bucketValue.put(MILLIS_ONE_HOUR, BUCKET_ONE_HOUR);
		bucketValue.put(MILLIS_FOUR_HOURS, BUCKET_FOUR_HOURS);
		bucketValue.put(MILLIS_TWELVE_HOURS, BUCKET_TWELVE_HOURS);
		bucketValue.put(MILLIS_TWENTY_FOUR_HOURS, BUCKET_TWENTY_FOUR_HOURS);
		bucketValue.put(MILLIS_ONE_WEEK, BUCKET_ONE_WEEK);
		bucketValue.put(MILLIS_ONE_MONTH, BUCKET_ONE_MONTH);
		return bucketValue;
	}
	
}
