package appnomic.mobile_monitoring.util;

import lombok.Data;

@Data
public class WSResponse {
	private Object responseStatus;
	private Object message;
	private Object data;
}
