package appnomic.mobile_monitoring.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;

public class BigQueryService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BigQueryService.class);
	
	private static BigQueryService bigQueryService = null;
	
	public BigQuery bigQuery;
	
	private BigQueryService() {
		try {
		File file = new File("sample-appnomic-bf25672d5456.json");
		InputStream input = new FileInputStream(file);
		bigQuery = BigQueryOptions.newBuilder()
							.setCredentials(ServiceAccountCredentials.fromStream(input))
							.build()
							.getService();
		} catch(IOException exc) {
			LOGGER.error("Could not establish BigQuery Connection", exc);
		}
	}
	
	public static BigQueryService getInstance() {
		if(null == bigQueryService) {
			bigQueryService = new BigQueryService();
		}
		return bigQueryService;
	}
}
