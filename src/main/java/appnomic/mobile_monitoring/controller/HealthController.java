package appnomic.mobile_monitoring.controller;

import spark.Request;
import spark.Response;
import spark.Route;

public class HealthController {
	public static Route showApiHealth = (Request request, Response response) -> {
        return "Success";
    };
}
