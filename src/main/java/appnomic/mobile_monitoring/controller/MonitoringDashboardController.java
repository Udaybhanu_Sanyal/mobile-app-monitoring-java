package appnomic.mobile_monitoring.controller;

import spark.Route;
import appnomic.mobile_monitoring.service.MonitoringDashboardService;
import appnomic.mobile_monitoring.service.PerformanceMonitoringService;
import spark.Request;
import spark.Response;

public class MonitoringDashboardController {
	public static Route getAllData = (Request request, Response response) -> {
		return PerformanceMonitoringService.getAllData();
	};
	public static Route getAppVersions = (Request request, Response response) -> {
		return MonitoringDashboardService.getAppVersions();
	};
	public static Route getTopErrorFiles = (Request request, Response response) -> {
		return MonitoringDashboardService.getTopErrorFiles(request);
	};
	public static Route getTopSlowPages = (Request request, Response response) -> {
		return MonitoringDashboardService.getTopSlowPages(request);
	};
	public static Route getSlowPagesTrend = (Request request, Response response) -> {
		return MonitoringDashboardService.getSlowPagesTrend(request);
	};
	public static Route getErrorFilesTrend = (Request request, Response response) -> {
		return MonitoringDashboardService.getErrorFilesTrend(request);
	};
	public static Route getLatestCrashedAppVersions = (Request request, Response response) -> {
		return MonitoringDashboardService.getLatestCrashedAppVersions(request);
	};
	public static Route getVersionCrashTrend = (Request request, Response response) -> {
		return MonitoringDashboardService.getVersionCrashTrend(request);
	};
	
}
