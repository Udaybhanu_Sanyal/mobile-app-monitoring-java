package appnomic.mobile_monitoring.controller;

import appnomic.mobile_monitoring.service.CrashMonitoringService;
import spark.Request;
import spark.Response;
import spark.Route;

public class CrashMonitoringDashboardController {
	public static Route getCrashOverview = (Request request, Response response) -> {
		return CrashMonitoringService.getCrashOverview(request);
	};
}
