package appnomic.mobile_monitoring.service;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

import appnomic.mobile_monitoring.model.CrashOverview;
import appnomic.mobile_monitoring.util.AppConstants;
import appnomic.mobile_monitoring.util.BigQueryService;
import appnomic.mobile_monitoring.util.WSResponse;
import spark.Request;

public class CrashMonitoringService {
	private static final Logger LOGGER = LoggerFactory.getLogger(CrashMonitoringService.class);

	public static Object getCrashOverview(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery Obj: "+ bigQuery);
		
		String intervalValue = request.queryParams("interval-value");
		String intervalUnit = request.queryParams("interval-unit");
		Long pastIntervalValue = (Long.parseLong(intervalValue) * 2);
		
		String query = "SELECT issue_id AS issueId, COUNT(issue_id) AS issueCount, COUNTIF(is_fatal=true) AS fatalCount, (COUNT(issue_id)-COUNTIF(is_fatal=true)) AS nonFatalCount" + 
				"		FROM " + AppConstants.CRASHLYTICS_TABLE +
				"		WHERE TIMESTAMP_DIFF(CURRENT_TIMESTAMP, event_timestamp, " + intervalUnit + ") < " + intervalValue +
				"		GROUP BY issueId";
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		
		
		String queryPastComparison = 	"SELECT COUNT(issue_id) AS issueCount " + 
										"FROM " + AppConstants.CRASHLYTICS_TABLE +
										"WHERE TIMESTAMP_DIFF(event_timestamp, TIMESTAMP_SUB(CURRENT_TIMESTAMP, INTERVAL " + pastIntervalValue + " " + intervalUnit +"), "+ intervalUnit +") < " + intervalValue;
		
		QueryJobConfiguration queryPastComparisonConfig = QueryJobConfiguration
															.newBuilder(queryPastComparison)
															.setUseLegacySql(false).build();
		
		TableResult result = null;
		TableResult pastResult = null;
		Long uniqueCount = 0L;
		Long fatalCount = 0L;
		Long nonFatalCount = 0L;
		Long crashCount = 0L;
		Long previousCrashCount = 0L;
		CrashOverview crashOverview = new CrashOverview();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			pastResult = bigQuery.query(queryPastComparisonConfig);
			
			Iterator<FieldValueList> resultIterator = result.iterateAll().iterator();
			while(resultIterator.hasNext()) {
				FieldValueList value = resultIterator.next();
				uniqueCount++;
				fatalCount += value.get("fatalCount").getLongValue();
				nonFatalCount += value.get("nonFatalCount").getLongValue();
			}
			crashCount = fatalCount + nonFatalCount;
			
			previousCrashCount = pastResult.iterateAll().iterator().next().get("issueCount").getLongValue();
			Long crashCountChange = crashCount - previousCrashCount;
			String percentageChange = String.format("%.2f", crashCountChange.doubleValue()/crashCount.doubleValue()*100.00);
			
			crashOverview.setFatalCrashes(fatalCount);
			crashOverview.setNonFatalCrashes(nonFatalCount);
			crashOverview.setUniqueCrashes(uniqueCount);
			crashOverview.setTotalCrashes(crashCount);
			crashOverview.setPercentageChange(percentageChange+"%");
			
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(crashOverview);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in CrashMonitoringService.getCrashOverview");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
}
