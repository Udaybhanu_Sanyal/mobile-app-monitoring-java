package appnomic.mobile_monitoring.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.JobException;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

import appnomic.mobile_monitoring.model.TopErrorFile;
import appnomic.mobile_monitoring.model.TopSlowPage;
import appnomic.mobile_monitoring.model.VersionWiseCrash;
import appnomic.mobile_monitoring.util.AppConstants;
import appnomic.mobile_monitoring.util.BigQueryService;
import appnomic.mobile_monitoring.util.WSResponse;
import spark.Request;

public class MonitoringDashboardService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringDashboardService.class);

	public static Object getAppVersions() {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery Obj: "+ bigQuery);
		
		String query =	"SELECT app_build_version AS appBuildVersion " +
					    "FROM" + AppConstants.PERFMON_TABLE +
					    "GROUP BY appBuildVersion " +
					    "ORDER BY appBuildVersion DESC";
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		
		TableResult result = null;
		WSResponse response = new WSResponse();
		List<Long> appVersions = new ArrayList<>();
		try {
			result = bigQuery.query(queryConfig);
			result.iterateAll().forEach(value -> {
				appVersions.add(value.get("appBuildVersion").getLongValue());
			});
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(appVersions);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getAppVersion");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	public static Object getTopErrorFiles(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery Obj: "+ bigQuery);
		
		String appBuildVersion = getInParameter(request.queryParams("app-build-version"));
		String fromTime = request.queryParams("from-time");
		String toTime = request.queryParams("to-time");
		Integer rowLimit = Integer.parseInt(request.queryParams("row-limit"));
		
		String query = 	"SELECT issue_id AS issueId, issue_title AS issueTitle, COUNT (issue_id) AS noOfIssues " + 
						"FROM " + AppConstants.CRASHLYTICS_TABLE + 
						"WHERE application.build_version IN " + appBuildVersion + " " + 
						"AND event_timestamp > TIMESTAMP_MILLIS("+ fromTime +") " + 
						"AND event_timestamp < TIMESTAMP_MILLIS(" + toTime +") "  + 
						"GROUP BY issue_id, issue_title " + 
						"ORDER BY noOfIssues DESC ";
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		
		TableResult result = null;
		List<TopErrorFile> topErrorFilesResult = new ArrayList<>();
		List<TopErrorFile> topErrorFilesResponse = new ArrayList<>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			Long totalErrorCount = 0L;
			
			Iterator<FieldValueList> resultIterator = result.iterateAll().iterator();
			while(resultIterator.hasNext()) {
				FieldValueList resultValue = resultIterator.next();
				
				TopErrorFile topErrorFile = new TopErrorFile();

				topErrorFile.setIssueTitle(resultValue.get("issueTitle").getStringValue());
				topErrorFile.setNoOfIssues(resultValue.get("noOfIssues").getLongValue());
				totalErrorCount += resultValue.get("noOfIssues").getLongValue();
				
				topErrorFilesResult.add(topErrorFile);
			}
			Iterator<TopErrorFile> topErrorFilesIterator = topErrorFilesResult.iterator();
			while(topErrorFilesIterator.hasNext() && rowLimit > 0) {
				TopErrorFile topErrorFile = topErrorFilesIterator.next();
				TopErrorFile topErrorFileResult = new TopErrorFile();
				
				topErrorFileResult.setIssueTitle(topErrorFile.getIssueTitle());
				topErrorFileResult.setNoOfIssues(topErrorFile.getNoOfIssues());
				
				Long noOfIssues = topErrorFile.getNoOfIssues();
				String percentageOfTotal = String.format("%.2f", noOfIssues.doubleValue()/totalErrorCount.doubleValue()*100.00);
				topErrorFileResult.setPercentage(percentageOfTotal+"%");
				topErrorFilesResponse.add(topErrorFileResult);
				rowLimit--;
			}
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(topErrorFilesResponse);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getTopErrorFiles");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	public static Object getTopSlowPages(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery Obj: "+ bigQuery);
		
		String appBuildVersion = getInParameter(request.queryParams("app-build-version"));
		String fromTime = request.queryParams("from-time");
		String toTime = request.queryParams("to-time");
		Integer rowLimit = Integer.parseInt(request.queryParams("row-limit"));
		
		String query = 	"SELECT event_name AS eventName, COUNTIF(trace_info.screen_info.slow_frame_ratio > 0) AS noOfSlowPages "+ 
						"FROM"+ AppConstants.PERFMON_TABLE +
						"WHERE event_type LIKE 'SCREEN_TRACE' "+ 
						"AND app_build_version IN " + appBuildVersion + " " + 
						"AND event_timestamp > TIMESTAMP_MILLIS("+ fromTime +") " + 
						"AND event_timestamp < TIMESTAMP_MILLIS(" + toTime +") "  + 
						"GROUP BY eventName " +
						"ORDER BY noOfSlowPages DESC";
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		
		TableResult result = null;
		List<TopSlowPage> topSlowPagesResult = new ArrayList<>();
		List<TopSlowPage> topSlowPageResponse = new ArrayList<>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			Long totalSlowPageCount = 0L;
			
			Iterator<FieldValueList> resultIterator = result.iterateAll().iterator();
			while(resultIterator.hasNext()) {
				FieldValueList resultValue = resultIterator.next();
				
				TopSlowPage topSlowPage = new TopSlowPage();

				topSlowPage.setEventName(resultValue.get("eventName").getStringValue());
				topSlowPage.setNoOfSlowPages(resultValue.get("noOfSlowPages").getLongValue());
				totalSlowPageCount += resultValue.get("noOfSlowPages").getLongValue();
				
				topSlowPagesResult.add(topSlowPage);
			}
			
			Iterator<TopSlowPage> topErrorFilesIterator = topSlowPagesResult.iterator();
			while(topErrorFilesIterator.hasNext() && rowLimit > 0) {
				TopSlowPage topSlowPage = topErrorFilesIterator.next();
				TopSlowPage topSlowPageResult = new TopSlowPage();
				
				topSlowPageResult.setEventName(topSlowPage.getEventName());
				topSlowPageResult.setNoOfSlowPages(topSlowPage.getNoOfSlowPages());
				
				Long noOfSlowPages = topSlowPage.getNoOfSlowPages();
				String percentageOfTotal = String.format("%.2f", noOfSlowPages.doubleValue()/totalSlowPageCount.doubleValue()*100.00);
				topSlowPageResult.setPercentage(percentageOfTotal+"%");
				topSlowPageResponse.add(topSlowPageResult);
				rowLimit--;
			}
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(topSlowPageResponse);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getTopSlowPages");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		
		return response;
	}
	
	public static Object getSlowPagesTrend(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery obj : " + bigQuery);
		
		List<String> appBuildVersions = Arrays.asList(request.queryParams("app-build-version").split(","));
		String fromTime = request.queryParams("from-time");
		String toTime = request.queryParams("to-time");
		String intervalBucket = getIntervalBucket(fromTime, toTime);
		String intervalBucketUnit = intervalBucket.split(" ")[1];
		
		String query = 	" WITH hours as (" + 
						"	SELECT *" + 
						"	FROM UNNEST(GENERATE_TIMESTAMP_ARRAY(TIMESTAMP_TRUNC(TIMESTAMP_MILLIS("+fromTime+"), "+intervalBucketUnit+"), TIMESTAMP_MILLIS("+toTime+"), INTERVAL "+intervalBucket+")" + 
						"	) AS time )" + 
						"	SELECT" + 
						"		STRING(hours.time) AS time," + 
						"		COUNTIF(perfMon.trace_info.screen_info.slow_frame_ratio > 0) AS slowPages," + 
						"		perfMon.app_build_version AS appBuildVersion" + 
						"	FROM " + AppConstants.PERFMON_TABLE +" AS perfMon  " + 
						"	RIGHT JOIN hours ON TIMESTAMP_TRUNC(perfMon.event_timestamp, "+intervalBucketUnit+") = hours.time " + 
						"	GROUP BY time, appBuildVersion" + 
						"	ORDER BY 1" ;
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		TableResult result = null;
		Map<String, Long> slowPageCountMap = new HashMap<String, Long>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			result.iterateAll().forEach(value -> {
				
				if(!slowPageCountMap.keySet().contains(value.get("time").getStringValue())) {
					if(null != value.get("appBuildVersion").getValue() && appBuildVersions.contains(value.get("appBuildVersion").getStringValue())) {
						slowPageCountMap.put(value.get("time").getStringValue(), value.get("slowPages").getLongValue());
					} else {
						slowPageCountMap.put(value.get("time").getStringValue(), 0L);
					}
				} else {
					if(null != value.get("appBuildVersion").getValue() && appBuildVersions.contains(value.get("appBuildVersion").getStringValue())) {
						Long existingCount = slowPageCountMap.get(value.get("time").getStringValue());
						Long newCount = existingCount + value.get("slowPages").getLongValue();
						slowPageCountMap.put(value.get("time").getStringValue(), newCount);
					} else {
						slowPageCountMap.put(value.get("time").getStringValue(), 0L);
					}
					
				}
				
			});
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(slowPageCountMap);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getSlowPagesTrend");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	public static Object getErrorFilesTrend(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery obj : " + bigQuery);
		
		List<String> appBuildVersions = Arrays.asList(request.queryParams("app-build-version").split(","));
		String fromTime = request.queryParams("from-time");
		String toTime = request.queryParams("to-time");
		String intervalBucket = getIntervalBucket(fromTime, toTime);
		String intervalBucketUnit = intervalBucket.split(" ")[1];
		
		String query = 	" WITH hours as (" + 
						"	SELECT *" + 
						"	FROM UNNEST(GENERATE_TIMESTAMP_ARRAY(TIMESTAMP_TRUNC(TIMESTAMP_MILLIS("+fromTime+"), "+intervalBucketUnit+"), TIMESTAMP_MILLIS("+toTime+"), INTERVAL "+intervalBucket+")" + 
						"	) AS time )" + 
						"	SELECT" + 
						"		STRING(hours.time) AS time," + 
						"		COUNT(crashlytics.issue_id) AS errorFiles," + 
						"		crashlytics.application.build_version AS appBuildVersion" + 
						"	FROM " + AppConstants.CRASHLYTICS_TABLE +" AS crashlytics  " + 
						"	RIGHT JOIN hours ON TIMESTAMP_TRUNC(crashlytics.event_timestamp, "+intervalBucketUnit+") = hours.time " + 
						"	GROUP BY time, appBuildVersion" + 
						"	ORDER BY 1" ;
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		TableResult result = null;
		Map<String, Long> errorFileCountMap = new HashMap<String, Long>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			result.iterateAll().forEach(value -> {
				if(!errorFileCountMap.keySet().contains(value.get("time").getStringValue())) {
					if(null != value.get("appBuildVersion").getValue() && appBuildVersions.contains(value.get("appBuildVersion").getStringValue())) {
						errorFileCountMap.put(value.get("time").getStringValue(), value.get("errorFiles").getLongValue());
					} else {
						errorFileCountMap.put(value.get("time").getStringValue(), 0L);
					}
				} else {
					if(null != value.get("appBuildVersion").getValue() && appBuildVersions.contains(value.get("appBuildVersion").getStringValue())) {
						Long existingCount = errorFileCountMap.get(value.get("time").getStringValue());
						Long newCount = existingCount + value.get("errorFiles").getLongValue();
						errorFileCountMap.put(value.get("time").getStringValue(), newCount);
					} else {
						errorFileCountMap.put(value.get("time").getStringValue(), 0L);
					}
					
				}
			});
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(errorFileCountMap);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getErrorFilesTrend");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	public static Object getLatestCrashedAppVersions(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery obj : " + bigQuery);
		
		Integer rowLimit = Integer.parseInt(request.queryParams("row-limit"));
		
		String query = 	"SELECT application.build_version AS version" + 
						"	  	FROM " + AppConstants.CRASHLYTICS_TABLE +
						"      	GROUP BY version" + 
						"      	ORDER BY version DESC LIMIT " + rowLimit;
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		TableResult result = null;
		List<Long> lastCrashedAppVersions = new ArrayList<>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			result.iterateAll().forEach(value -> {
				lastCrashedAppVersions.add(value.get("version").getLongValue());
			});
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(lastCrashedAppVersions);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getLatestCrashedAppVersions");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	public static Object getVersionCrashTrend(Request request) {
		BigQuery bigQuery = BigQueryService.getInstance().bigQuery;
		System.out.println("bigQuery obj : " + bigQuery);
		
		List<String> appBuildVersions = Arrays.asList(request.queryParams("app-build-version").split(","));
		String fromTime = request.queryParams("from-time");
		String toTime = request.queryParams("to-time");
		String intervalBucket = getIntervalBucket(fromTime, toTime);
		String intervalBucketUnit = intervalBucket.split(" ")[1];
		
		String query = 	" WITH hours as (" + 
						"	SELECT *" + 
						"	FROM UNNEST(GENERATE_TIMESTAMP_ARRAY(TIMESTAMP_TRUNC(TIMESTAMP_MILLIS("+fromTime+"), "+intervalBucketUnit+"), TIMESTAMP_MILLIS("+toTime+"), INTERVAL "+intervalBucket+")" + 
						"	) AS time )" + 
						"SELECT " + 
						"    STRING(hours.time) AS time," + 
						"    COUNT(crashlytics.event_id) AS crashes," + 
						"    crashlytics.application.build_version AS appBuildVersion " + 
						"FROM " + AppConstants.CRASHLYTICS_TABLE +  " AS crashlytics " +
						"    RIGHT JOIN hours ON TIMESTAMP_TRUNC(crashlytics.event_timestamp, "+intervalBucketUnit+") = hours.time " + 
						"GROUP BY time, appBuildVersion " + 
						"ORDER BY 1";
		
		QueryJobConfiguration queryConfig = QueryJobConfiguration
											.newBuilder(query)
											.setUseLegacySql(false).build();
		TableResult result = null;
		Map<Long, Map<String, Long>> versionWiseCrashMap = new HashMap<>();
		WSResponse response = new WSResponse();
		try {
			result = bigQuery.query(queryConfig);
			for(String appBuildVersion : appBuildVersions) {
				Map<String, Long> versionWiseCrash = new HashMap<>();
				result.iterateAll().forEach(value -> {
					// VersionWiseCrash versionWiseCrash = new VersionWiseCrash();
					
					if(!versionWiseCrash.keySet().contains(value.get("time").getStringValue())) {
						if(null != value.get("appBuildVersion").getValue() && value.get("appBuildVersion").getLongValue() == Long.parseLong(appBuildVersion)) {
							versionWiseCrash.put(value.get("time").getStringValue(), value.get("crashes").getLongValue());
						} else {
							versionWiseCrash.put(value.get("time").getStringValue(), 0L);
						}
					} else {
						if(null != value.get("appBuildVersion").getValue() && value.get("appBuildVersion").getLongValue() == Long.parseLong(appBuildVersion)) {
							Long existingCount = versionWiseCrash.get(value.get("time").getStringValue());
							Long newCount = existingCount + value.get("crashes").getLongValue();
							versionWiseCrash.put(value.get("time").getStringValue(), newCount);
						}
					}
				});
				versionWiseCrashMap.put(Long.parseLong(appBuildVersion), versionWiseCrash);
			}
			
			response.setResponseStatus("SUCCESS");
			response.setMessage("SUCCESS");
			response.setData(versionWiseCrashMap);
		} catch (JobException | InterruptedException exc) {
			LOGGER.info("Error in MonitoringService.getVersionCrashTrend");
			LOGGER.error("Interruption occured while fetching data", exc);
			response.setResponseStatus("FAILURE");
			response.setMessage(exc.getMessage());
		}
		return response;
	}
	
	private static String getInParameter(String input) {
	    List<String> inputList = Arrays.asList(input.split(","));
	    String output = "";
	    String [] temp = new String [inputList.size()];
	    int tempCounter=0;
	    for(String s : inputList) {
	        temp[tempCounter] = "'" + s + "'";
	        tempCounter++;
	    }
	    output = "(" + String.join(",", temp) + ")";
	    return output;
	}
	
	private static String getIntervalBucket(String fromTime, String toTime) {
		// TODO Auto-generated method stub
		Long diffMillis = Long.parseLong(toTime) - Long.parseLong(fromTime);
		return AppConstants.getBucketMap().get(diffMillis);
	}
	
}
