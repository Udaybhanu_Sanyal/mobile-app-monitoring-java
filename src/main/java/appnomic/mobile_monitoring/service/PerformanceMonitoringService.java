package appnomic.mobile_monitoring.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.BigQuery;
import com.google.cloud.bigquery.BigQueryOptions;
import com.google.cloud.bigquery.QueryJobConfiguration;
import com.google.cloud.bigquery.TableResult;

public class PerformanceMonitoringService {


	public static Object getAllData() {
		
		try {
//			bigQuery = BigQueryOptions.newBuilder()
//					// .setProjectId("sample-appnomic")
//					.setCredentials(ServiceAccountCredentials
//							.fromStream(new FileInputStream("sample-appnomic-bf25672d5456.json")))
//					.build().getService();
			
//			new FileInputStream("sample-appnomic-bf25672d5456.json")
			File file = new File("sample-appnomic-bf25672d5456.json");
			InputStream input = new FileInputStream(file);
			BigQuery bigQuery = BigQueryOptions.newBuilder()
								.setCredentials(ServiceAccountCredentials.fromStream(input))
								.build()
								.getService();
			
			// BigQuery bigQuery = BigQueryOptions.getDefaultInstance().getService();
			
			System.out.println("bigquery object : " + bigQuery);
			QueryJobConfiguration queryConfig = QueryJobConfiguration
					.newBuilder("select * from `sample-appnomic.firebase_performance.org_codejudge_android_ANDROID`")
					.setUseLegacySql(false).build();

			// Create a job ID so that we can safely retry.
			// String jobName = "jobId_" + UUID.randomUUID().toString();
			// JobId jobId = JobId.of(UUID.randomUUID().toString());
			// JobId jobId = JobId.newBuilder().setJob(jobName).build();
			
			// Create Job with jobId
			
			
			// bigQuery.create(JobInfo.of(jobId, queryConfig));
			// System.out.println("queryJob : " + queryJob);
			
			// Job queryJob = bigQuery.getJob(jobId);

			// queryJob = queryJob.waitFor(); // RetryOption.maxAttempts(3),
											// RetryOption.totalTimeout(Duration.ofMillis(30000L))

			// Checking for errors
//			if (jobId.getJob().equals(queryJob.getJobId().getJob())) {
//				// throw new RuntimeException("Job no longer exists");
//				System.out.println("Job created successfully");
//			} else {// if (null != queryJob.getStatus().getError()) {
//				// throw new RuntimeException(queryJob.getStatus().getError().toString());
//				System.out.println("Job was not created");
//			}

			// Getting the results
			TableResult result = bigQuery.query(queryConfig);
			// result = queryJob.getQueryResults();
			result.iterateAll().forEach(rows -> rows.forEach(row -> System.out.println(row.getValue())));
//			for (FieldValueList row : result.getValues()) {
//				System.out.println(row);
//			}

		}
		catch (InterruptedException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	
}
