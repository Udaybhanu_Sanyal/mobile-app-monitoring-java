package appnomic.mobile_monitoring.model;

import lombok.Data;

@Data
public class VersionWiseCrash {
	private String hour;
	private Long crashes;
	private Long appBuildVersion;
}
