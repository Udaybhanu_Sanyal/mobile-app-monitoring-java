package appnomic.mobile_monitoring.model;

import lombok.Data;

@Data
public class AppBuildVersion {
	private String appBuildVersion;
}
