package appnomic.mobile_monitoring.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TopSlowPage {
	@JsonProperty("name")
	private String eventName;
	@JsonProperty("count")
	private Long noOfSlowPages;
	@JsonProperty("percentage")
	private String percentage;
}
