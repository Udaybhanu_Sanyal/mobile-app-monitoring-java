package appnomic.mobile_monitoring.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TopErrorFile {
	// private String issueId;
	@JsonProperty("name")
	private String issueTitle;
	@JsonProperty("count")
	private Long noOfIssues;
	@JsonProperty("percentage")
	private String percentage;
}
