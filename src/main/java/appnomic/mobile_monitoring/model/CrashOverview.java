package appnomic.mobile_monitoring.model;

import lombok.Data;

@Data
public class CrashOverview {
	private Long uniqueCrashes;
	private Long totalCrashes;
	private Long fatalCrashes;
	private Long nonFatalCrashes;
	private String percentageChange;
}
