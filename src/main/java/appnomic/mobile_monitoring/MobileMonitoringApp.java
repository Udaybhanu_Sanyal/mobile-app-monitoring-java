package appnomic.mobile_monitoring;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.debug.DebugScreen.enableDebugScreen;

import appnomic.mobile_monitoring.controller.CrashMonitoringDashboardController;
import appnomic.mobile_monitoring.controller.HealthController;
import appnomic.mobile_monitoring.controller.MonitoringDashboardController;
import appnomic.mobile_monitoring.util.JsonTransformer;

/**
 * Hello world!
 *
 */
public class MobileMonitoringApp {
	
	public static void main(String[] args) {
		port(4567);
    	enableDebugScreen();
    	
    	path("/api", () -> {
    		get("/health",          				HealthController.showApiHealth, new JsonTransformer());
    		
    		get("/perfMonData",						MonitoringDashboardController.getAllData, new JsonTransformer());
    		
    		get("/app-versions",					MonitoringDashboardController.getAppVersions, new JsonTransformer());
    		get("/top-error-files",					MonitoringDashboardController.getTopErrorFiles, new JsonTransformer());
    		get("/top-slow-pages",					MonitoringDashboardController.getTopSlowPages, new JsonTransformer());
    		get("/slow-pages-trend",				MonitoringDashboardController.getSlowPagesTrend, new JsonTransformer());
    		get("/error-files-trend",				MonitoringDashboardController.getErrorFilesTrend, new JsonTransformer());
    		get("/latest-crashed-app-versions",		MonitoringDashboardController.getLatestCrashedAppVersions, new JsonTransformer());
    		get("/version-crash-trend",				MonitoringDashboardController.getVersionCrashTrend, new JsonTransformer());
    		
    		get("/crash-overview",					CrashMonitoringDashboardController.getCrashOverview, new JsonTransformer());
    		
    	});
    	
    	
//    	Client Validations
    	
//    	before((request, response) -> {
//    	    boolean authenticated;
//    	    // ... check if authenticated
//    	    if (!authenticated) {
//    	        halt(401, "You are not welcome here");
//    	    }
//    	});
    	
//    	after((request, response) -> {
//    	    response.header("foo", "set by after filter");
//    	});
	}
	
}
